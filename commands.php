<?php

return [
    // TODO : Add list of commands here
    \Src\Commands\Power::class,
    \Src\Commands\Division::class,
    \Src\Commands\Multiplication::class,
    \Src\Commands\Subtraction::class,
    \Src\Commands\Addition::class
];
