<?php

namespace Src\Commands;

use Illuminate\Console\Command;

class Division extends Command
{
    protected $signature = 'divide {input?*}';

    protected $description = 'Divide all given Numbers';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        $input = $this->filterNumberOnly($this->argument('input'));
        $this->info($this->printInput($input) . ' = ' . $this->divideAllElements($input));
    }

    private function filterNumberOnly($array)
    {
        $res = array();
        for ($i = 0; $i <= sizeof($array); $i++) is_numeric($array[$i]) && array_push($res, +$array[$i]);
        return $res;
    }

    private function divideAllElements($array)
    {
        $res = $array[0];
        for ($i = 1; $i < sizeof($array); $i++) $res = $res / $array[$i];
        var_dump($res);
        return $res;
    }

    private function printInput($array)
    {
        $res;
        for ($i = 0; $i < sizeof($array); $i++) $res .= ($i < sizeof($array) - 1) ?  $array[$i]. ' / ' : $array[$i];
        return $res;
    }
}
