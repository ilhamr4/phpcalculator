<?php

namespace Src\Commands;

use Illuminate\Console\Command;

class Subtraction extends Command
{
    protected $signature = 'subtract {input?*}';

    protected $description = 'Subtract all given Numbers';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        $input = $this->filterNumberOnly($this->argument('input'));
        $this->info($this->printInput($input) . ' = ' . $this->subtractAllElements($input));
    }

    private function filterNumberOnly($array)
    {
        $res = array();
        for ($i = 0; $i <= sizeof($array); $i++) is_numeric($array[$i]) && array_push($res, +$array[$i]);
        return $res;
    }

    private function subtractAllElements($array)
    {
        $res = $array[0];
        for ($i = 1; $i < sizeof($array); $i++) $res = $res - $array[$i];
        return $res;
    }

    private function printInput($array)
    {
        $res;
        for ($i = 0; $i < sizeof($array); $i++) $res .= ($i < sizeof($array) - 1) ?  $array[$i]. ' - ' : $array[$i];
        return $res;
    }
}
